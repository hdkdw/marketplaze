const orderItemRepo = require('../repository/orderItemRepo')
const promoRepo = require('../repository/promoRepo')
const productRepo = require('../repository/productRepo')
const checkOutHelper = require('../utility/checkoutService')

class OrderItemService {

    async getAllOrderItemByUserId(req,res) {
        let userId = req.body.userId
        try {
            const result = await orderItemRepo.getAllOrderItemByUserId(userId);
            res.status(200).json({order: result}); 
        } catch(e) {
            console.log(e); 
            res.sendStatus(500);
        }
    }

    async createOrderItemByUserId(req,res) {
        let { customerOrderId, userId, productId, quantity } = req.body
        
        //promoType
        try{
            let promoType =''

            const checkProduct = await promoRepo.checkProduct(productId);
            const productIdPriceData = await productRepo.getProductIdPriceData(productId)
            
            if(!checkProduct[0].checks){
                promoType='-';

                // checkOutHelper(quantity, unitPrice, minBuy, freeItem)
                let summaryItemOrder1 = checkOutHelper(quantity,promoType, productIdPriceData[0].unit_price, 0, 0 )

                const result1 = await orderItemRepo
                            .createOrderItemByUserId(
                                customerOrderId, 
                                userId, 
                                productId, 
                                quantity, 
                                promoType, 
                                summaryItemOrder1.subtotal,
                                summaryItemOrder1.discount,
                                summaryItemOrder1.total
                                );
                let sendResponse1 ={
                    customerOrderId : customerOrderId, 
                    userId : userId, 
                    productId : productId, 
                    quantity : quantity, 
                    promo : promoType, 
                    subtotal : summaryItemOrder1.subtotal,
                    discount : summaryItemOrder1.discount,
                    total : summaryItemOrder1.total
                }
                res.status(200).json(
                {
                    status : 'true',
                    message : 'new_order_item created',
                    data : sendResponse1
                }); 
            
            } else {
                const getPromoData = await promoRepo.getPromoType(productId);
            
                if(!getPromoData[0].promo){
                promoType='-'
                    } else {
                    promoType=getPromoData[0].promo
                }

                // subtotal, discount, total
                // checkOutHelper(quantity, promoType, unitPrice, minBuy, freeItem)
                let summaryItemOrder = checkOutHelper(quantity,promoType, getPromoData[0].unit_price, getPromoData[0].min_buy, getPromoData[0].free_item )
                const result = await orderItemRepo
                            .createOrderItemByUserId(
                                customerOrderId, 
                                userId, 
                                productId, 
                                quantity, 
                                promoType, 
                                summaryItemOrder.subtotal,
                                summaryItemOrder.discount,
                                summaryItemOrder.total
                                );
                let sendResponse ={
                    customerOrderId : customerOrderId, 
                    userId : userId, 
                    productId : productId, 
                    quantity : quantity, 
                    promo : promoType, 
                    subtotal : summaryItemOrder.subtotal,
                    discount : summaryItemOrder.discount,
                    total : summaryItemOrder.total
                }
                res.status(200).json(
                {
                    status : 'true',
                    message : 'new_order_item created',
                    data : sendResponse
                }); 
            }   
                        
        } catch(err) {
            console.log(err); 
            res.sendStatus(500);
        }
    }


    async getAllOrderItemByOrderId2(req,res){

        let { customerOrderId, userId} = req.body;

        try{
            
            const result = await orderItemRepo.getAllOrderItemByOrderId(customerOrderId, userId);
                res.status(200).json(
                    {
                        status : 'true',
                        message : 'list all order_item by order_id',
                        data : result
                    }); 
    
        } catch(err){
            console.log(err); 
            res.sendStatus(500);
        }
        
    }
}

module.exports = OrderItemService