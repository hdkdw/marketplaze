const productRepo = require('../repository/productRepo')


 class ProductService {

    async getAllProduct(req,res) {
        try {
            const result = await productRepo.getAllProduct();
            res.status(200).json({products: result}); 
        } catch(e) {
            console.log(e); 
            res.sendStatus(500);
        }
    }

    async getOnlyProductWithPromo(req,res) {
        try {
            const result = await productRepo.getOnlyProductWithPromo();
            res.status(200).json({products: result}); 
        } catch(e) {
            console.log(e); 
            res.sendStatus(500);
        }
    }

}

module.exports = ProductService