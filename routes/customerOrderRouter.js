var express = require('express');
var router = express.Router();

const CustomerOrderService = require('../services/customerOrderService')
const customerOrderServices = new CustomerOrderService

router.get('/orderbyUserId', customerOrderServices.getAllOrderByUserId);
router.post('/createcustomerorder', customerOrderServices.createOrderByUserId);
router.get('/getCheckOutSummary', customerOrderServices.getCheckOutSummary)
router.post('/createCheckOutSummary', customerOrderServices.createCheckout)




module.exports = router;