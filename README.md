# MARKETPLAZE API


API untuk melakukan proses melihat list produk, memilih produk dan melakukan checkout pada sebuah marketplace. 

## Asumsi & Dependensi 
1. Tech stack menggunakan Node JS dan MySQL 
2. Service yang dibuat untuk proses melihat list produk, memilih produk dan melakukan checkout. 
3. Tidak menggunakan authentication 
4. Tidak menggunakan payment service pada proses checkout 

## Cara Menggunakan 
1. Clone repo dan running di lokal dengan perintah di konsol : ```npm run start``` 
2. Running menggunakan docker : 
 ```docker build -t node-app/marketplaze .``` 
 ```docker run -p3000:3000 node-app/marketplaze```   
 
## User Guide 
1. EndPoint API terkait produk ada pada file : ```routes/productRouter.js``` 
2. EndPoint API terkait order_item ada pada file : ```routes/orderItemRouter.js``` 
3. EndPoint API terkait customer_order dan proses checkout ada pada file : ```routes/customerOrderRouter.js``` 

## Saran Pengembangan 
1. integrasi proses authentication 
2. feature cart untuk menampung sementara item produk yang akan dibeli user 
3. Penambahan feature promo dengan metode discount rate 
4. feature invoice 
5. feature notification (in apps & e-mail) 
6. feature user sign up 
7. feature RBAC  
8. arsitektur microservice 
 







