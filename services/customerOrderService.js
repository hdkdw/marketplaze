const customerOrderRepo = require('../repository/customerOrderRepo')

class OrderService {

    async getAllOrderByUserId(req,res) {
        try {
            const result = await customerOrderRepo.getAllOrderByUserId(1);
            res.status(200).json({order: result}); 
        } catch(e) {
            console.log(e); 
            res.sendStatus(500);
        }
    }

    

    async createOrderByUserId(req,res) {
        let userId = req.body.userId
        try {
            const result = await customerOrderRepo.createOrderByUserId(userId);
            let customer_order_id = result.insertId
            let sendResponse ={
                "userId" : userId,
                 "customer_order_id" : customer_order_id
            }
            res.status(200).json(
                {
                    status : 'true',
                    message : 'new_cutomer_order created',
                    data : sendResponse
                }); 
        } catch(e) {
            console.log(e); 
            res.sendStatus(500);
        }
    }


    async getCheckOutSummary(req,res) {
        let { customerOrderId, userId} = req.body

        try{

            const result = await customerOrderRepo.getCheckOutSummary(customerOrderId,userId);
            res.status(200).json(
            {
                status : 'true',
                message : 'checkout summary',
                data : result
            }); 

        } catch (err){
            console.log(err); 
            res.sendStatus(500);
        }

    }


    async createCheckout(req,res){
        let { customerOrderId, userId} = req.body
            
        try{

            const checkoutSummary = await customerOrderRepo.getCheckOutSummary(customerOrderId,userId);

            const createCheckout = await customerOrderRepo
                                    .createCheckout(
                                        checkoutSummary[0].TotalItem,
                                        checkoutSummary[0].Subtotal, 
                                        checkoutSummary[0].Discount, 
                                        checkoutSummary[0].GrandTotal,
                                        customerOrderId, 
                                        userId);

            res.status(200).json(
                {
                    status : 'true',
                    message : 'create checkout',
                    data : {
                        createCheckOut : createCheckout,
                        chekoutSummary :checkoutSummary
                    }
                        
                }); 

        } catch (err){
            console.log(err); 
            res.sendStatus(500)
        }                     


    }


}

module.exports = OrderService