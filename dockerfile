FROM hdwic/centos8node14:node14

WORKDIR /home/node/app

COPY package*.json ./

RUN npm install

COPY . /home/node/app

EXPOSE 3000

CMD ["npm", "start"]



